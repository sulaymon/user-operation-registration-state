package com.tenet.hackaton;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserOperationRegistrationStateEntity {
    private Integer userId;
    private Integer operationId;
    private List<FieldDescription> fields;

}

package com.tenet.hackaton;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserOperationRegistrationStateHolder {
    Map<Integer, Map<Integer, List<FieldDescription>>> user2OperationState = new HashMap<>();

    @PostConstruct
    void initHolder() {
        //заполняем данные из бд
        Map<Integer, List<FieldDescription>> operationState = new HashMap<>();
        operationState.put(1, new ArrayList<>());
        user2OperationState.put(69, operationState);
    }

    public void saveCurrentFieldData(List<FieldDescription> fields, int userId, int operationId) {
        user2OperationState.get(userId)
                .get(operationId)
                .addAll(fields);
    }

    public List<FieldDescription> getCurrentOperationRegistrationState(int userId, int operationId) {
        return user2OperationState.get(userId).get(operationId);
    }


}

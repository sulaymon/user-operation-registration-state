package com.tenet.hackaton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class OperationStateController {

    @Autowired
    UserOperationRegistrationStateHolder userOperationRegistrationStateHolder;

    @PostMapping("/userOperationRegistrationState/saveCurrentFieldData/{userId}/{operationId}")
    public void saveCurrentFieldData(@RequestBody List<FieldDescription> fieldDescription, @PathVariable("userId") Integer userId,
                                     @PathVariable("operationId") Integer operationId) {
        userOperationRegistrationStateHolder.saveCurrentFieldData(fieldDescription, userId, operationId);
    }

    @GetMapping(value = "/currentOperationRegistrationState/{userId}/{operationId}")
    public List<FieldDescription> getCurrentOperationRegistrationState(@PathVariable("userId") Integer userId,
                                                                       @PathVariable("operationId") Integer operationId) {
        return userOperationRegistrationStateHolder.getCurrentOperationRegistrationState(userId, operationId);
    }
}